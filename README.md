# Builder

## Технические характеристики:

- nodejs 8.9.1
- pug, stylus
- gulp 3.9.1

Глобальные зависимости:
```
$ npm i -g gulp
$ npm i -g bower
```


## Перед началом проекта
```
$ npm install
$ bower install
```

## Запуск
```
$ npm start // or gulp [params]

```










