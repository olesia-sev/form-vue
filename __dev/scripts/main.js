;'use strict';

(function (LS, Vue, VueMask) {
    Vue.directive('mask', VueMask.VueMaskDirective);

    const mixin = {
            data() {
                return {
                    isEdit: false,
                    backgroundColor: '#fff',
                }
            },
            methods: {
                edit() {
                    this.isEdit = true;
                    this.backgroundColor = '#f4f4f4';
                    this.padding = '10';
                },
                onMouseOver() {
                    this.backgroundColor = '#f4f4f4';
                },
                onMouseOut() {
                    this.backgroundColor = '#fff';
                }
            }
        },
        Name = {
            template: '#personal-info-name',
            mixins: [mixin],
            data() {
                return {
                    value: LS.getItem('personalInfoName') || ''
                }
            },
            methods: {
                save() {
                    this.isEdit = false;
                    LS.setItem('personalInfoName', this.value);
                }
            }
        },

        Surname = {
            template: '#personal-info-surname',
            mixins: [mixin],
            data() {
                return {
                    value: LS.getItem('personalInfoSurname') || ''
                }
            },
            methods: {
                save() {
                    this.isEdit = false;
                    LS.setItem('personalInfoSurname', this.value);
                }
            }
        },

        Patronymic = {
            template: '#personal-info-patronymic',
            mixins: [mixin],
            data() {
                return {
                    value: LS.getItem('personalInfoPatronymic') || ''
                }
            },
            methods: {
                save() {
                    this.isEdit = false;
                    LS.setItem('personalInfoPatronymic', this.value);
                }
            }
        },

        Birthday = {
            template: '#personal-info-birthday',
            mixins: [mixin],
            data() {
                return {
                    value: LS.getItem('personalInfoBirthday') || ''
                }
            },
            methods: {
                save() {
                    this.isEdit = false;
                    LS.setItem('personalInfoBirthday', this.value);
                }
            }
        },

        Phone = {
            template: '#contacts-phone',
            mixins: [mixin],
            data() {
                return {
                    value: LS.getItem('contactsPhone') || ''
                }
            },
            methods: {
                save() {
                    this.isEdit = false;
                    LS.setItem('contactsPhone', this.value);
                }
            }
        },

        Email = {
            template: '#contacts-email',
            mixins: [mixin],
            data() {
                return {
                    value: LS.getItem('contactsEmail') || ''
                }
            },
            methods: {
                save() {
                    this.isEdit = false;
                    LS.setItem('contactsEmail', this.value);
                }
            }
        },

        Password = {
            template: '#contacts-password',
            mixins: [mixin],
            data() {
                return {
                    value: LS.getItem('contactsPassword') || ''
                }
            },
            methods: {
                save() {
                    this.isEdit = false;
                    LS.setItem('contactsPassword', this.value);
                }
            }
        },

        Address = {
            template: '#address',
            mixins: [mixin],
            data() {
                return {
                    addressList: [{
                        id: 1,
                        fullAddress: '432 071 Россия, г. Троицк, ул. Ленина д.34, кв.54',
                        checked: true
                    }, {
                        id: 2,
                        fullAddress: '445 086 Россия, г.Керчь, ул. Первого Интернационала д.14, кв.6',
                        checked: false
                    }],
                    addresses: [
                        '432 071 Россия, г. Троицк, ул. Ленина д.34, кв.54',
                        '445 086 Россия, г.Керчь, ул. Первого Интернационала д.14, кв.6'
                    ],
                    checked: 0
                }
            },
            methods: {
                remove(address) {
                    this.addressList.splice(this.addressList.indexOf(address), 1);
                }
            }
        };

    return new Vue({
        el: '#app',
        data() {
            return {
                value: ''
            };
        },
        components: {
            'personal-info-name': Name,
            'personal-info-surname': Surname,
            'personal-info-patronymic': Patronymic,
            'personal-info-birthday': Birthday,
            'contacts-phone': Phone,
            'contacts-email': Email,
            'contacts-password': Password,
            'address-component': Address
        }
    });
})(localStorage, Vue, VueMask);
