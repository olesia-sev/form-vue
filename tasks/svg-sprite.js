import gulp from 'gulp';
import svgstore from 'gulp-svgstore';
import rename from 'gulp-rename';
import cheerio from 'gulp-cheerio';
import svgmin from 'gulp-svgmin';

gulp.task('svg-sprite', function () {
    gulp.src('./__dev/images/icons/*.svg')
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[data-name]').removeAttr('data-name');
                $('[stroke]').removeAttr('stroke');
                $('[title]').removeAttr('title');
            },
            parserOptions: {xmlMode: true}
        }))
        .pipe(svgmin())
        .pipe(rename({prefix: 'icon-'}))
        .pipe(svgstore())
        // .pipe(gulp.dest('./__dev/images'));
        .pipe(gulp.dest('./dist/template/images'));
});

// import gulp from 'gulp';
// import svgSprite from 'gulp-svg-sprites';
// import plumber from 'gulp-plumber';
// import fs from 'fs';
//
// gulp.task('svg-sprite', () => {
//     gulp.src('./__dev/sprites/svg/*.svg')
//         .pipe(svgSprite({
//             cssFile: "../styles/helpers/08_svg-sprite.styl",
//             svg: {
//                 sprite: "../images/sprite-icons.svg"
//             },
//             svgPath: './images/sprite-icons.svg',
//             preview: false,
//             padding: 5,
//             templates: {
//                 css: fs.readFileSync(process.cwd() + '/__dev/styles/helpers/svg-sprite-template.css', "utf-8")
//             },
//             svgId: "svg-%f"
//         }))
//         .pipe(plumber())
//         .pipe(gulp.dest('./__dev/sprites'))
// });
//
