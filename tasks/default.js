import runSequence from 'run-sequence';
import gulp from 'gulp';

gulp.task('default', () => (
	runSequence(
		'clean',
		['sprite', 'svg-sprite'],
		['pug', 'bower', 'scripts'],
		[
			'styles:vendor',
			'styles',
			'copy-image',
			'fonts',
			'favicons',
			'index-page'
		],
		'server',
		'watch'
	)
));